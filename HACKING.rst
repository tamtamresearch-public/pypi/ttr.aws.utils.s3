Development setup
=================

Prerequisites:

- python packages installed (system-wide):

  - pdm (for building the package)
  - uv (python and venv management)
  - nox (for build and test automation)
  - proclamation (for documenting changes)

To start developing::

  $ uv sync

Then activate created virtualenv, e.g.::

  $ source .venv/bin/activate

Implement a change
==================

- Make sure the issue/feature/change has relevant issue in issue tracker, e.g. issue # 123
- Implement the change in code (commit it if needed)
- test the code by `nox -s test`, fix possible issues, enhance test suite if needed
- Add so called proclamation fragment:

  - find relevent `changes` subfolder, e.g. `changes/features`
  - create the proclamation fragment, e.g. `changes/features/issue.123.md`. Make the text is user oriented (leave other details to issue tracker).

- put the proclamation fragment under source control
- if you like, preview new part of the CHANGELOG.md by::

  $ proclamation draft

- push the change

Release HOWTO
=============

To make a release, 

Update CHANGELOG.md
-------------------

Make sure, relevant proclamation fragment(s) are present in the `changes` subfolders.

Preview new section of CHANGELOG.md::

  $ proclamation draft

and edit fragments if needed.

Decide new version, e.g. `9.9.9`.

Updated the CHANGELOG.md, e.g.::

  $ proclamation build 9.9.9 --overwrite --delete-fragments

Read the new CHANGELOG.md and manually edit if necessary.

Commit the CHANGELOG.md

Tag the version and test it
---------------------------

Tag the latest commit to desired version, e.g.::

  $ git tag -a 9.9.9

Run tests::

  $ nox -s test

If there are any issues, remove the git tag and fix the issue. Finally ensure the git tag is on the latest commit.

Build the package
-----------------
::

  $ uv build

The package will be located in `dist` folder.

If needed, test the package in any means.

Publish the package to test.pypi.org
------------------------------------
::

  $ uv publish -r testpypi

When asked for username use `vlcinsky`.

Use the username/password for test.pypi.org - it differs from the `pypi.org` credentials.

Preview the published package on https://test.pypi.org/project/ttr-aws-utils-s3/

Test installation of the updated package from testpypi::

  $ pipx install --index-url https://test.pypi.org/simple/ --pip-args="--extra-index-url=https://pypi.org/simple/" --suffix @test9.9.9 ttr.aws.utils.s3

Publish the package to pypi.org
-------------------------------
::

  $ uv publish

When asked for username use `vlcinsky`.

Push the changes to GitLab
--------------------------
::

  $ git push --follow-tags
