import nox

nox.options.default_venv_backend = "uv|venv"

PYTHON_ALL_VERSIONS = ["3.8", "3.9", "3.10", "3.11", "3.12", "3.13"]

# PYTHON_DEFAULT_VERSION="3.11"
@nox.session(python=PYTHON_ALL_VERSIONS)
def test(session):
    session.install("-e", ".")
    session.run("s3lsvers",  "-h")
    session.run("s3tmpgen",  "-h")
    session.run("s3getvers",  "-h")
    session.install("-r", "test_requirements.txt")
    session.run("pytest", "-sv", "tests")
